package com.ehrc.user.dao;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ehrc.db.FormData;
import com.ehrc.utility.HibernateUtil;

public class FormDataDAOImpl implements FormDataDAO {

	Logger logger = LoggerFactory.getLogger(FormDataDAOImpl.class);

	@Override
	public String saveFormData(FormData formObj){
		Session ses = null;
		Transaction tx = null;
        String response = "";
		// get Session
		ses = HibernateUtil.getSession();
		
		if(formObj == null) {
			logger.info(" People Object is null");
			
		} else {
			
			try	{
				formObj.setCreated_at(new Date());
				logger.info("Created Date ="+formObj.getCreated_at());
				
				tx = ses.beginTransaction();
				ses.saveOrUpdate(formObj);
				logger.info("Record created");
				tx.commit();
				
				response = "{message : Record Saved Successfully}";
				
				
			} catch (Exception e) {
				logger.error("User record creation problem", e);
				tx.rollback();
				response = null;

			} finally {
				HibernateUtil.closeSession();
			}
		}
		return response;
	}
}
