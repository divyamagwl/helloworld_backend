package com.ehrc.user.dao;

import com.ehrc.db.FormData;

public interface FormDataDAO {

	String saveFormData(FormData formObj);

}
